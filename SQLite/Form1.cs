﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;

namespace SQLite
{
    public partial class Form1 : Form
    {
        String[] basesd;
        private string directorio;
        private string bd;
        private SQLiteConnection con;
        private SQLiteCommand cmd;
        private SQLiteDataAdapter DA;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader SEL;

        public Form1()
        {
            InitializeComponent();
        }
      


    
        private void Form1_Load(object sender, EventArgs e)
        {
         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            try
            {
                folderBrowserDialog1.ShowDialog();
                label5.Text = "Folder actual  " + folderBrowserDialog1.SelectedPath;
                basesd = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.db");
                
                directorio = folderBrowserDialog1.SelectedPath;
                foreach (var item in basesd)
                {
                    comboBox1.Items.Add(Path.GetFileName(item));
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la ruta ");
            }


        }

    


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bd = comboBox1.Text.ToString();
            comboBox2.Items.Clear();


            try
            {
                // MessageBox.Showdirectorio+ "\\" + bd);
                con = new SQLiteConnection("Data Source=" + directorio + "\\" + bd + ";");
                con.Open();
                cmd = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type = \"table\";", con);
                SEL = cmd.ExecuteReader();

                if (SEL.HasRows)
                {
                    comboBox2.Enabled = true;

                    while (SEL.Read())
                    {
                        comboBox2.Items.Add(SEL.GetString(0));
                    }

                    comboBox2.Text = comboBox2.Items[0].ToString();

                }


                con.Close();
            }
            catch (Exception error)
            {

               MessageBox.Show("Error al seleccionar las tablas" + error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String Sqlquery = "SELECT * FROM " + comboBox2.Text;

            try
            {
                con = new SQLiteConnection("Data Source=" + directorio + "\\" + bd + ";");
                con.Open();
                cmd = new SQLiteCommand(Sqlquery, con);
                cmd.ExecuteNonQuery();

                SQLiteDataAdapter tablaqsl = new SQLiteDataAdapter(cmd);

                DataTable tablaMostrar = new DataTable();

                tablaqsl.Fill(tablaMostrar);

                dataGridView1.DataSource = tablaMostrar;


                //Cragamos tab

                con.Close();


            }
            catch (Exception error)
            {

                MessageBox.Show("Error al seleccionar los datos de la tabla" + error);
            }


        }
    }
}

